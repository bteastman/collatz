import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;

public class CollatzOrganizer {

	public static void main(String[] args) throws IOException {
		CollatzCommonMedia cloud = new CollatzCommonMedia();
		cloud.iteratorSaveFile = new RandomAccessFile("cruncherSave.txt", "rw");
		
		String importedIterator=cloud.iteratorSaveFile.readLine().trim().replace(""+(char)(0), "");
		cloud.iterator = new BigInteger(importedIterator);
		
		int numberOfProcessors = Runtime.getRuntime().availableProcessors();
		//int numberOfProcessors=2;
		int cruncherRange=10000;
		CollatzCruncher[] threads = new CollatzCruncher[numberOfProcessors];
		while (true)
		{
			long startTime = System.currentTimeMillis();
			for (int i=0; i < threads.length; i++)
			{
				threads[i]=new CollatzCruncher(cloud.iterator.add(new BigInteger(cruncherRange*i+"")), cruncherRange);
			}
			
			for (int i=0; i < threads.length; i++)
			{
				if (!threads[i].isComplete())
				{
					i=0;
					Thread.yield();
				}
			}
			long stopTime=System.currentTimeMillis();
			long totalTime=(stopTime-startTime)+1;
			long numberOfIterationsCompleted=cruncherRange*numberOfProcessors;
			long rate=numberOfIterationsCompleted*1000/totalTime;
			cloud.iterator = cloud.iterator.add(new BigInteger(numberOfIterationsCompleted+""));
			
			cloud.iteratorSaveFile.seek(0);
			cloud.iteratorSaveFile.writeChars(cloud.iterator.toString());
			System.out.println(cloud.iterator+"\t\t"+rate+" iterations/second");
			
		}
	}
}
