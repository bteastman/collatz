import java.math.BigInteger;

public class CollatzCruncher extends Thread{
	private BigInteger beginningIndex;
	private int range;
	private boolean complete;
	
	public CollatzCruncher(BigInteger i, int r)
	{
		beginningIndex=i;
		range=r;
		complete=false;
		this.start();
	}

	public void run(){
		for (BigInteger iterator = beginningIndex; iterator.subtract(beginningIndex).longValue() < range; iterator=iterator.add(BigInteger.ONE))
		{
			BigInteger currentIteration = iterator;
			while ( !currentIteration.equals(BigInteger.ONE))
			{
				
				if (currentIteration.mod(BigInteger.TWO).equals(BigInteger.ZERO))
					currentIteration=currentIteration.divide(BigInteger.TWO);
				else 
				{
					currentIteration = currentIteration.multiply(new BigInteger("3"));
					currentIteration = currentIteration.add(BigInteger.ONE);
				}
			}
		}
		complete=true;
	}
	
	public boolean isComplete()
	{
		return complete;
	}

}
