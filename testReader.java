import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.math.BigInteger;
import java.util.Arrays;

public class testReader {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		RandomAccessFile raf = new RandomAccessFile("cruncherSave.txt", "rw");
		
		BigInteger start = new BigInteger("2").pow(68);
		System.out.println(start.toString());
		raf.setLength(0);
		raf.writeChars(start.toString());
		
		raf.seek(0);
		String all = raf.readLine().trim().replace(""+(char)(0), "");
		System.out.println("Output Length: "+all.length());
		System.out.println("Output: "+all);
	}	

}
